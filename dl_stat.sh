#!/bin/bash

proxychains wget https://wordpress.org/plugins/$1/stats -qO-| grep UserDownloads | awk -F: '{print $2}' | awk -F \" '{print $1}'