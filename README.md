This is the SQL dump of a database I created of XSS in wordpress plugins.  A lot of these are context dependent
and need manual evaluation.  The column auto_verify is set to 1 if the vulnerability was confirmed with a working
PoC exploit.

To import the database:

mysql> create database wpvulndb

$ mysql -u root -p < wpvulndb


I gave a presentation at Boston Bsides in May 2016, here are the slides:
http://www.slideshare.net/LarryCashdollar/how-to-discover-1352-wordpress-plugin-0days-in-one-hour-not-really